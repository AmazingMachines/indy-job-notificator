# indy-job-notificator
## What is
* Track your indy corp's finished jobs by sending them to a discord webhook

![](https://cdn.discordapp.com/attachments/565204778045538314/842669943069343764/unknown.png)

## How do
* This app doesn't run on a loop, you have to put it on a scheduler, anything above five minutes is fine
* You do not need a server to run this, you can run it locally if you want
* You do need some other way to get your character tokens
* Your tokens must be in JSON format

__You can use this to get them:__
https://gitlab.com/AmazingMachines/flask-esipy-example-am

It is preconfigured for this

---

__Install dependancies__
```shell
pip install -r requirements.txt
```

## Create your app in https://developers.eveonline.com

1. Go to https://developers.eveonline.com
2. Login and go to `manage applications`
3. Create a new application
4. Fill all the fields

__Requirements__
* The character registered must have the `Factory_Manager` role ingame
* For the scopes, you will need:
* `esi-wallet.read_character_wallet`
* `esi-search.search_structures.v1`
* `esi-corporations.read_facilities.v1`
* `esi-universe.read_structures.v1`
* `esi-industry.read_corporation_jobs.v1`

## APP Configuration

1. Copy and rename the `config.dist` to `config.py`
2. Edit it.

```python
# Fill these lines with the data you get from https://developers.eveonline.com
ESI_SECRET_KEY = ''  # your secret key
ESI_CLIENT_ID = ''  # your client ID
ESI_USER_AGENT = 'NAME' # your ESI application name
```

3. Copy and rename the `dot.env` to `.env`
4. Edit it.

```python
# The token of the character must be in JSON format
TOKEN = {"access_token": "XXX", "expires_in": 1234, "token_type": "Bearer", "refresh_token": "XXX", "CHARACTER_ID": 4321}
WEBHOOK = "" # your discord webhook goes here
BOTNAME = "" # set bot name to override standard webhook name, or leave it empty
```
5. Have fun.
