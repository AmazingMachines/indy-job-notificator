# industry-notificator.py
# pylint: disable=C

import json
import os
import sqlite3
import sys
from dotenv import load_dotenv
import requests
from esipy import EsiApp, EsiClient, EsiSecurity

import config


# CHECK IF TRANQUILITY IS RUNNING BEFORE DOING ANYTHING
r = requests.get('https://esi.evetech.net/latest/status/?datasource=tranquility')
if r.status_code != 200:
    print("\n\nTRANQUILITY IS DOWN - NOTIFICATOR WILL NOT RUN")
    sys.exit()


print("\n\nLoading ESIPY...")


load_dotenv()
TOKEN = os.getenv('TOKEN')
BOTNAME = os.getenv('BOTNAME')
WEBHOOK = os.getenv('WEBHOOK')
tokens = json.loads(TOKEN)


# create the app
esiapp = EsiApp().get_latest_swagger


# init the security object
esisecurity = EsiSecurity(
    redirect_uri=config.ESI_CALLBACK,
    client_id=config.ESI_CLIENT_ID,
    secret_key=config.ESI_SECRET_KEY,
    headers={'User-Agent': config.ESI_USER_AGENT}
)

# init the client
esiclient = EsiClient(
    retry_requests=True,
    security=esisecurity,
    cache=None,
    raw_body_only=False,
    headers={'User-Agent': config.ESI_USER_AGENT}
)

# update the security object,
esisecurity.update_token({
    'access_token': '',  # leave this empty
    'expires_in': -1,  # seconds until expiry, so we force refresh anyway
    'refresh_token': tokens['refresh_token']
})



conn = sqlite3.connect(os.path.dirname(os.path.realpath(__file__))+'/notifid.db')
c = conn.cursor()


def get_logged_character_id():
    return tokens['CHARACTER_ID']


def esi_get_char_corp_id(_char_id):
    esiop = esiapp.op['get_characters_character_id'](character_id  = _char_id)
    response = esiclient.request(esiop).data
    return response.corporation_id


def esi_post_universe_names_ids(_any_id):
    esiop = esiapp.op['post_universe_names'](ids  = _any_id)
    response = esiclient.request(esiop).data
    return response


def esi_get_universe_structure_name(_struct_id):
    op = esiapp.op['get_universe_structures_structure_id'](structure_id  = _struct_id)
    response = esiclient.request(op).data
    return response.name


############################################################################################################
############################################################################################################


def esi_corp_industry_jobs_multi(_corpID):
    esiop = esiapp.op['get_corporations_corporation_id_industry_jobs'](corporation_id = _corpID, include_completed = True)
    res = esiclient.head(esiop)
    if res.status == 200:
        print(res.status)
        number_of_page = res.header['X-Pages'][0]
        print(number_of_page)
        operations = []
        for page in range(1, number_of_page + 1):
            operations.append(esiapp.op['get_corporations_corporation_id_industry_jobs'](corporation_id = _corpID, page = page, include_completed = True))
        results = esiclient.multi_request(operations)
        return results
    else:
        return None


def esi_corp_industry_jobs(_corpID):
    esiop = esiapp.op['get_corporations_corporation_id_industry_jobs'](corporation_id = _corpID)
    res = esiclient.head(esiop)
    if res.status == 200:
        print(res.status)
        esiresponse = esiclient.request(esiop)
        return esiresponse.data
    else:
        return None


##############################################################################################################################
##############################################################################################################################


def check_if_notif_table():
    stmt = "SELECT name FROM sqlite_master WHERE type='table' AND name='notifications'"
    c.execute(stmt)
    result = c.fetchone()
    if result:
        print('DB TABLE ALREADY EXISTS')
    else:
        print('TABLE DOES NOT EXIST -- CREATING DB')
        c.execute('''CREATE TABLE notifications(notification_id INTEGER)''')
        conn.commit()


def insert_db_notifid(_notifid):
    # insert the notification id to the database
    c.execute('''SELECT * FROM notifications WHERE notification_id=?''', (_notifid,))
    exists = c.fetchall()
    if not exists:
        # Note the added comma after _notifid
        c.execute("INSERT INTO notifications VALUES (?)", (_notifid,))
        conn.commit()


def check_if_notification_exists(_notifid):
    # check the database to see if the notification has already been shown
    c.execute('''SELECT * FROM notifications WHERE notification_id=?''', (_notifid,))
    exists = c.fetchall()
    if exists:
        return True
    else:
        return False


##############################################################################################################################
##############################################################################################################################


def send_to_discord(_text = "",
                    _title = "",
                    _jobs = "",
                    _time = "",
                    _portrait = "",
                    _bprints = ""):

    url_list = [WEBHOOK]

    data = {}
    data["username"] = BOTNAME
    data["content"] = _text
    data["embeds"] = []
    embed = {}
    embed["title"] = "⏰   " + _title
    embed["thumbnail"] = {}
    embed["thumbnail"]["url"] = _portrait


    embed["fields"] = [{"name": _jobs,
                        "value": _bprints,
                        "inline": False},
                        ]


    embed["footer"] = {"text": _time}

    embed["color"] = 0x0066ff
    data["embeds"].append(embed)

    for url in url_list:
        result = requests.post(url, data=json.dumps(data), headers={"Content-Type": "application/json"})
        try:
            result.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print(err)
        else:
            print("Payload delivered successfully, code {}.".format(result.status_code))








print("ESIPY loaded.")







if __name__ == '__main__':

    from time import sleep
    from datetime import datetime, timezone
    import dateutil.parser

    actions =  {1: "Build",
                2: "Research Tech",
                3: "T-E",
                4: "M-E",
                5: "Copy",
                6: "Duplicate",
                7: "Reverse",
                8: "Invent",
                9: "React"}

    check_if_notif_table()
    current_datetime = datetime.now(timezone.utc)
    print(current_datetime)

    corpid = esi_get_char_corp_id(get_logged_character_id())
    joblist = esi_corp_industry_jobs(corpid)

    if joblist is None:
        print("TRANQUILITY UNRESPONSIVE / MISSING ROLES - EXITING")
        sys.exit()

    TOTALJOBS = len(joblist)

    idlist = []
    for i in joblist:
        idlist.append(i.installer_id)
        idlist.append(i.blueprint_type_id)

    namelist = esi_post_universe_names_ids(idlist)


    discordlist = []

    for i in joblist:
        for k in namelist:
            if i.installer_id == k.id:
                toon = k.name
            if i.blueprint_type_id == k.id:
                bprint = k.name


        esidate = dateutil.parser.isoparse(str(i.end_date))


        if esidate < current_datetime:
            statustxt = 'READY'
            state = True
        else:
            statustxt = 'RUNNING'
            state = False

        notifid = i.job_id

        if not check_if_notification_exists(notifid) and state == True:
            discordlist.append((toon, bprint, actions[i.activity_id], i.installer_id, i.facility_id, i.runs, esidate))
            insert_db_notifid(notifid)


##############################################################################################################


    atoons = []
    for i in discordlist:
        if i[0] not in atoons:
            atoons.append(i[0])

    discordlist.sort(key=lambda tup: tup[4])

    toondict = {}
    for activetoon in atoons:
        toondict[activetoon] = ""
        ctr = 0
        factorylist = []
        for x in discordlist:
            if x[0] == activetoon:
                jobtime = x[6]
                ctr += 1
                portrait = "https://images.evetech.net/characters/" + str(x[3]) + "/portrait"
                if (activetoon, x[4]) not in factorylist:
                    toondict[activetoon] += "`" + esi_get_universe_structure_name(x[4]) + ":`\n" + x[2] +  " x" + str(x[5]) + " " + x[1] + "\n"
                    factorylist.append((activetoon, x[4]))

                else:
                    toondict[activetoon] += x[2] +  " x" + str(x[5]) + " " + x[1] + "\n"
                print(factorylist)


        finishedjobs = ctr

        send_to_discord(_text = "",
                        _time = jobtime.strftime("%H:%M:%S") + " ET  (" + str(TOTALJOBS) + " corp jobs running or not delivered)",
                        _jobs = "{} jobs completed".format(str(finishedjobs)),
                        _portrait = portrait,
                        _title = activetoon,
                        _bprints = toondict[activetoon])
        sleep(0.5)


    conn.close()






"""
actions =  {1: "Manufacturing",
            2: "Researching Technology",
            3: "Time Productivity",
            4: "Material Productivity",
            5: "Copying",
            6: "Duplicating",
            7: "Reverse Engineering",
            8: "Invention",
            9: "Reaction"}
"""



